<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Services\Implementation;
use App\Models\Announcement;
use App\Services\Interfaces;

use App\Services\Interfaces\IAnnouncementServiceInterface;

class AnnouncementServiceImpl implements Interfaces\IAnnouncementServiceInterface {
    private $model;
    function  __construct(){
        $this->model = new Announcement();
    }


    function getAnnouncement()
    {
        return $this->model
            ->with("area")->get();
    }

    function getAnnouncementById(int $id)
    {
        return $this->model->where('id',$id)->get();

    }

    function postAnnouncement(array $announcement)
    {
        $this->model->create($announcement);
    }

    function putAnnouncement(array $announcement, int $id)
    {
        $this->model->where('id',$id)
            ->first()
            ->fill($announcement)
            ->save();
    }

    function delAnnouncement(int $id)
    {
        $announcement=$this->model->where('id',$id)
            ->first();
        if($announcement != null){
            $announcement->delete();
        }

    }

    function restoreAnnouncement(int $id)
    {
        // TODO: Implement restoreAnnouncement() method.
    }
}

