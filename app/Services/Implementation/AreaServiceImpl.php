<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Services\Implementation;
use App\Services\Interfaces;
use App\Models\Area;
use App\Services\Interfaces\IAreaServiceInterface;

class AreaServiceImpl implements Interfaces\IAreaServiceInterface {
    private $model;
    function  __construct(){
        $this->model = new Area();
    }


    function getArea()
    {
        return $this->model->get();
    }

    function getAreaById(int $id)
    {
        // TODO: Implement getAreaById() method.
    }

    function postArea(array $area)
    {
        $this->model->create($area);
    }

    function putArea(array $area, int $id)
    {
        $this->model->where('id',$id)
            ->first()
            ->fill($area)
            ->save();
    }

    function delArea(int $id)
    {
        $area=$this->model->where('id',$id)
            ->first();

        if($area != null){
            $area->delete();
        }
    }

    function restoreArea(int $id)
    {
        // TODO: Implement restoreArea() method.
    }
}

