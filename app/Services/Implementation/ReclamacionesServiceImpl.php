<?php

namespace App\Services\Implementation;
use App\Services\Interfaces;
use App\Models\Libroreclamaciones;
class ReclamacionesServiceImpl implements Interfaces\IReclamacionesServiceInterface{
    
    private $model;
    function  __construct(){
        $this->model = new Libroreclamaciones();
    }
    
    

    public function getReclamaciones() {
//        return $this->model->withTrashed()->get();
        return $this->model->get();
    }

    public function getReclamacionesById(int $id) {
        return $this->model->where('id',$id)->get();
    }
    /*
     * crea un nueva reclamacion en el sistema
     */
    function postReclamaciones(array $reclamaciones) {
       return $this->model->create($reclamaciones)->id;
    }

    public function putReclamaciones(array $user, int $id) {
        $this->model->where('id',$id)
                ->first()
                ->fill($user)
                ->save();
    }
    public function delReclamaciones(int $id) {
        $reclamaciones = $this->model->find($id);
        
        if($reclamaciones != null){
            $reclamaciones->delete();
        }
    }
    public function restoreReclamaciones(int $id) {
        $reclamaciones = $this->model->withTrashed()->find($id);
        
        if($reclamaciones != null){
            $reclamaciones->restore();
        }
    }

}
