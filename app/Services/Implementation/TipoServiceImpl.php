<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Services\Implementation;
use App\Services\Interfaces;
use App\Models\Tipo;
class TipoServiceImpl implements Interfaces\ITipoServiceInterface{
    private $model;
    function  __construct(){
        $this->model = new Tipo();
    }        

    public function delTipo(int $id) {
         $tipo=$this->model->where('id',$id)
                ->first();
        
        if($tipo != null){
            $tipo->delete();
        }
    }

    public function getTipo() {
        return $this->model->get();
    }

    public function getTipoById(int $id) {
         return $this->model->where('id',$id)->get();
    }

    public function postTipo(array $tipo): void {
        $this->model->create($tipo);
    }

    public function putTipo(array $tipo, int $id) {
        $this->model->where('id',$id)
                ->first()
                ->fill($tipo)
                ->save();
    }

    public function putTipoId(string $field, string $value, string $id) {
        //        if ($field=='firstname'){ $field='nombre';}
//        if ($field=='lastname'){ $field='apellido';}
//        if ($field=='cf_1267'){ $field='grado';}
//        if ($field=='cf_1269'){ $field='cargoactual';}
//        if ($field=='cf_1271'){ $field='experiencia';}
        
        $this->model->where('recordId',$id)->update(array($field =>$value));
    }
        
    public function restoreTipo(int $id) {
         $tipo = $this->model->withTrashed()->find($id); 
        if($tipo != null){
            $tipo->restore();
        }
    }
    

}
    
