<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Services\Implementation;
use App\Services\Interfaces;
use App\Models\Docentes;
class DocentesServiceImpl implements Interfaces\IDocentesServiceInterface{
    private $model;
    function  __construct(){
        $this->model = new Docentes();
    }
            
    public function delDocentes(int $id) {
        $docente=$this->model->where('recordId',$id)
                ->first();
        
        if($docente != null){
            $docente->delete();
        }
    }

    public function getDocentes() {
        return $this->model->get();

    }

    public function getDocentesById(int $id) {
        return $this->model->where('recordId',$id)->get();
    }

    public function postDocentes(array $docentes): void {
        $this->model->create($docentes);
    }

    public function putDocentes(array $docentes, int $id) {
        $this->model->where('recordId',$id)
                ->first()
                ->fill($docentes)
                ->save();
    }

    public function putDocentesId(string $field, string $value, string $id) {
        $this->model->where('recordId',$id)->update(array($field =>$value));
    }

    public function restoreDocentes(int $id) {
        $docente = $this->model->withTrashed()->find($id);
        
        if($docente != null){
            $docente->restore();
        }
    }

}
    
