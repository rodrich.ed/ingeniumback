<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Services\Implementation;
use App\Services\Interfaces;
use App\Models\Categoria;
class CategoriaServiceImpl implements Interfaces\ICategoriaServiceInterface{
    private $model;
    function  __construct(){
        $this->model = new Categoria();
    }

    public function restoreDocentes(int $id) {
        $categoria = $this->model->withTrashed()->find($id);

        if($categoria != null){
            $categoria->restore();
        }
    }

    public function delCategoria(int $id) {
          $categoria=$this->model->where('id',$id)
                ->first();

        if($categoria != null){
            $categoria->delete();
        }
    }

    public function getCategoria() {
          return $this->model->get();

    }

    public function getCategoriaById(int $id) {
          return $this->model->where('id',$id)->get();

    }

    public function postCategoria(array $categoria): void {
                $this->model->create($categoria);

    }

    public function putCategoria(array $categoria, int $id) {
        $this->model->where('id',$id)
                ->first()
                ->fill($categoria)
                ->save();
    }

    public function putCategoriaId(string $field, string $value, string $id) {
//        if ($field=='firstname'){ $field='nombre';}
//        if ($field=='lastname'){ $field='apellido';}
//        if ($field=='cf_1267'){ $field='grado';}
//        if ($field=='cf_1269'){ $field='cargoactual';}
//        if ($field=='cf_1271'){ $field='experiencia';}

        $this->model->where('recordId',$id)->update(array($field =>$value));
    }


}

