<?php



namespace App\Services\Implementation;

use App\Services\Interfaces;
use Illuminate\Support\Facades\Log;

use App\Models\Productos;

use App\Models\Categoria;

use App\Models\Tipo;

use Illuminate\Support\Facades\DB;

class ProductosServiceImpl implements Interfaces\IProductosServiceInterface{



    private $model;

    function  __construct(){

        $this->model = new Productos();

    }



    public function delProductos(int $recordId) {

    $producto=$this->model->where('recordId',$recordId)

                ->first();

        if($producto != null){

            $producto->delete();

        }

    }



    public function getProductos($request) {

        $query = $this->model->select('nombreproducto', 'productos.id', 'id_categoria', 'id_tipo', 'link_imagen','productos.slug')->with('categoria','tipo');

        if($request->has('nombreproducto'))

            $query->where('nombreproducto','LiKE','%'.$request->nombreproducto.'%');

        if($request->has('partnumber'))

            $query->where('partnumber','LiKE','%'.$request->partnumber.'%');

        if($request->has('id_categoria'))

            $arrayCategory = array();

            $arrayCategory = explode(",", $request->id_categoria);

            if($arrayCategory[0] != ""){

		$query->join('categoria', 'categoria.id', '=', 'productos.id_categoria')

		->Where(function ($query) use($arrayCategory) {

			$query->whereIn('id_categoria', $arrayCategory)

			->orWhereIn('categoria.slug',$arrayCategory);

		});

                //$query->whereIn('id_categoria', $arrayCategory);

	

		}

        if($request->has('id_tipo'))

            $arrayType = array();

            $arrayType = explode(",", $request->id_tipo);

            if($arrayType[0] != "")

		$query->join('tipo', 'tipo.id', '=', 'productos.id_tipo')

                ->Where(function ($query) use($arrayType) {

                        $query->whereIn('id_tipo', $arrayType)

                        ->orWhereIn('tipo.nombremegamenu',$arrayType);

                });

                //$query->whereIn('id_tipo', $arrayType);

        return  $query->orderBy('nombreproducto','Asc')->orderBy('fechainicio','desc');

    }



    public function getProductosById(string $id) {

			$link= 'https://ingenium.edu.pe/ficha-matricula/#/v2?pid=';

			$id = urldecode($id);

			$productos= $this->model->where(function($query) use ($id){

				$query->where('slug',$id);

				$query->orWhere('id', $id);

			})->with('docente1','docente2','docente3','docente4','docente5','docente6','docente7','docente8','docente9','docente10','categoria','tipo')->get();

			for ($i=0;$i < count($productos);$i++){

				$url = MD5($productos[$i]['recordId'].$productos[$i]['partnumber']);

				$productos[$i]['link_pid']=$link.$url.'_15,';

				$productos[$i]['37bac3'] = $url;

			}

			return $productos;

    }



    public function postProductos(array $productos) {

		// First: search for any other product with the same product code.

		// For example:

		// 'PE GERPCON 2022 I ONLINE' and 'PE GERPCON 2021 IV ONLINE'

		// both share 'GERPCON', therefore '2021 IV' must be replaced with '2022 I'

        DB::beginTransaction();



		try {

            $previousProductVersion = Productos::where('nombreproducto','=',$productos['nombreproducto'])->orWhere('slug','=',$productos['slug']);

            if($previousProductVersion->get()->isEmpty()){

                // Second: search for the active_campaign tags ids related to the current product

                $activeCampaignProductTag = DB::table('active_campain_tag')->select('id')->whereRaw("length(tag) > 4 and '".$productos['partnumber']."' like concat('%',substr(tag,2),'%')")->first();

                $activeCampaignProductCategoryTag = DB::table('active_campain_tag')->join('categoria',\DB::raw("replace(categoria.nombre,' ','')"),'like',\DB::raw("concat('%',substr(active_campain_tag.tag,2),'%') collate utf8mb4_general_ci"))->whereRaw('length(tag) > 4')->where('categoria.id','=',$productos['id_categoria'])->select('active_campain_tag.id')->first();

                if($activeCampaignProductTag) $productos['active_campaign_product_tag_id'] = $activeCampaignProductTag->id;

                if($activeCampaignProductCategoryTag) $productos['active_campaign_area_tag_id'] = $activeCampaignProductCategoryTag->id;

                $previousProductVersion = $this->model->create($productos);

            }

            else $previousProductVersion->first()->fill($productos)->save();
		DB::commit();

            return $previousProductVersion->get();

        } catch (Exception $e) {

            DB::rollBack();

            return ['status'=>'error','message' => 'Error en la base de datos'];

        }

    }



    public function putProductos(array $productos, int $id) {

			$this->model->where('recordId',$productos['recordId'])

							->first()

							->fill($productos)

							->save();

    }



    public function restoreProductos(int $id) {

        $producto = $this->model->withTrashed()->find($id);



        if($producto != null){

            $producto->restore();

        }

    }



    public function putProductosId(string $field, string $value, string $id) {

			$this->model->where('recordId',$id)->update(array($field =>$value));

    }



    public function getProductosByCategoria(array $productos) {

        header('Content-type: application/json');

        $response=[];

        $contador=0;

        if(count($productos['categoria'])>0 && count($productos['tipo'])>0){

            for ( $i = 0; $i < count($productos['categoria']); $i++) {

                for ( $j = 0; $j < count($productos['tipo']); $j++) {

                    $consulta = $this->model->where(['id_tipo'=>$productos['tipo'][$j],'id_categoria'=>$productos['categoria'][$i]])->with('categoria','tipo')->get();

                    if(count($consulta)>0){

                        for($k = 0; $k < count($consulta); $k++){

                         array_push($response,$consulta[$k]);

                        }

                    }

                }

            }

        }



        if(count($productos['categoria'])==0 && count($productos['tipo'])>0 ){

            $contador =count($productos['tipo']);

            for ( $i = 0; $i < $contador; $i++) {



                $consultado=$this->model->where('id_tipo',$productos['tipo'][$i])->with('categoria','tipo')->get();



                if(count($consultado)>0){

                    for($j = 0; $j < count($consultado); $j++){

                         array_push($response,$consultado[$j]);

                    }

                }

            }

        }

        if(count($productos['tipo'])==0 && count($productos['categoria'])>0 ){

            $contador =count($productos['categoria']);



            for ( $i = 0; $i < $contador; $i++) {



                $consultado=$this->model->where('id_categoria',$productos['categoria'][$i])->with('categoria','tipo')->get();



                if(count($consultado)>0){

                    for($j = 0; $j < count($consultado); $j++){

                         array_push($response,$consultado[$j]);

                    }

                }

            }

        }

        return json_encode($response);

    }



    public function getMegamenu() {

                 $response = Categoria::select(['id','nombre','colorprimario','colorsecundario','image_code','slug'])->orderBy('nombre','Asc')->get();



		$tipos= Tipo::all();

		#$response=array();



		for ( $i = 0; $i < count($response); $i++) {

			for($j=0; $j < count($tipos); $j++){

				$productos = Productos::select('id','nombreproducto','fechainicio','slug')

				->where(['id_categoria'=>$response[$i]['id'],'id_tipo'=>$tipos[$j]['id']])

				->orderBy('nombreproducto','Asc')

				->orderBy('fechainicio','desc')->get();#->unique(array('nombreproducto','id_tipo'));

				$filteredProducts = array();

				if(sizeof($productos) > 0){

					if($tipos[$j]['id'] == 4) $productos[0]->nombreproducto = 'Taller de Especialización para la '.$productos[0]->nombreproducto;

					$currentProductName = $productos[0]->nombreproducto;

					$firstMatch = true;

					foreach ($productos as $indx => $prod) {

						if($indx == 0) array_push($filteredProducts,$prod);

						if($currentProductName != $prod->nombreproducto){

							array_push($filteredProducts,$prod);

							$currentProductName = $prod->nombreproducto;

						}

					}

				}

				if($tipos[$j]['id'] == 4) $response[$i]['programas']=array_merge($response[$i]['programas'], $filteredProducts);

				$response[$i][$tipos[$j]['nombremegamenu']]=$filteredProducts;

			}

		}

		return $response;

    }



    public function getRecientes(int $id_categoria,int $id_programa_actual) {

			$week_earlier = gmdate('Y-m-d',strtotime('-1 week'));

            $result = $this->model->select('id','nombreproducto','link_imagen','fechainicio','fechafin','slug')

                ->where('id_categoria',$id_categoria)

				->whereDate('fechainicio','>=',$week_earlier)

				->where('id','<>',$id_programa_actual)



				->orderBy('fechainicio','asc')

				->orderBy('nombreproducto')->get();#->unique('nombreproducto');

				$filteredProducts = array();

				if(sizeof($result) > 0){

					$currentProductName = $result[0]->nombreproducto;

					foreach ($result as $indx => $prod) {

						if($indx == 0) array_push($filteredProducts,$prod);

						if($currentProductName != $prod->nombreproducto){

							array_push($filteredProducts,$prod);

							$currentProductName = $prod->nombreproducto;

						}

					}

				}else{

					$result = $this->model->select('id','nombreproducto','link_imagen','fechainicio','fechafin','slug')

							->where('id_categoria',$id_categoria)

							->where('id','<>',$id_programa_actual)

							->orderBy('fechainicio','asc')

							->orderBy('nombreproducto')->get();#->unique('nombreproducto');

					$filteredProducts = array();

					$currentProductName = $result[0]->nombreproducto;

					foreach ($result as $indx => $prod) {

							if($indx == 0) array_push($filteredProducts,$prod);

							if($currentProductName != $prod->nombreproducto){

									array_push($filteredProducts,$prod);

									$currentProductName = $prod->nombreproducto;

							}

					}

			}

			return $filteredProducts;

    }

    function findProductsById(int $id)

    {

//        return $this->model->where('id_categoria',$id)->get();

        $response = Categoria::select('id','nombre','colorprimario','colorsecundario','image_code')->where('id',$id)->get();

        $tipos= Tipo::all();



        for ( $i = 0; $i < count($response); $i++) {

            for($j=0; $j < count($tipos); $j++){



                $consulta = Productos::select('id','nombreproducto')->where(

                    ['id_categoria'=>$response[$i]['id'],'id_tipo'=>$tipos[$j]['id']])->get();



                $response[$i][$tipos[$j]['nombremegamenu']]=$consulta;

            }

        }

        return $response;

    }



}


