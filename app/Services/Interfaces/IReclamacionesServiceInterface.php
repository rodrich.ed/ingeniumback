<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services\Interfaces;


interface IReclamacionesServiceInterface
{
   function getReclamaciones();
   /**
    * 
    * @param int $id
    * @return User
    */
   function getReclamacionesById(int $id);
   /**
    * 
    * @param array $reclamaciones
    * @return void
    */
   function postReclamaciones(array $reclamaciones);
   function putReclamaciones(array $user, int $id);
   function delReclamaciones(int $id);
   
 
   function restoreReclamaciones(int $id);
   

}