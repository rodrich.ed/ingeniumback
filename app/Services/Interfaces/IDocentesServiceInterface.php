<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services\Interfaces;


interface IDocentesServiceInterface
{
   function getDocentes();
   /**
    * 
    * @param int $id
    * @return docentes
    */
   function getDocentesById(int $id);
   /**
    * 
    * @param array $docentes
    * @return void
    */
   function postDocentes(array $docentes);
   function putDocentes(array $docentes, int $id);
   function delDocentes(int $id);
   function putDocentesId(string $field, string $value, string $id );
   
 
   function restoreDocentes(int $id);
   

}