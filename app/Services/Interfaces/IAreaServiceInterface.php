<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services\Interfaces;


interface IAreaServiceInterface
{
    function getArea();
    /**
     *
     * @param int $id
     * @return area
     */
    function getAreaById(int $id);
    /**
     *
     * @param array $area
     * @return void
     */
    function postArea(array $area);
    function putArea(array $area, int $id);
    function delArea(int $id);


    function restoreArea(int $id);
//   function getMegamenu();

}
