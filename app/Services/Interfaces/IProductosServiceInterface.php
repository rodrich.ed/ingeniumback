<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services\Interfaces;


interface IProductosServiceInterface
{
   function getProductos($request);
   /**
    *
    * @param int $id
    * @return producto
    */
   function getProductosById(string $id);
   /**
    *
    * @param array $productos
    * @return void
    */
   function postProductos(array $productos);
   function getProductosByCategoria(array $productos);
   function putProductos(array $productos, int $id);
   function delProductos(int $id);
   function putProductosId(string $field, string $value, string $id );
   function findProductsById(int $id);

   function restoreProductos(int $id);
   function getMegamenu();
   function getRecientes(int $categoria_id, int $producto_id);

}
