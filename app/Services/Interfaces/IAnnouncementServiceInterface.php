<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services\Interfaces;


interface IAnnouncementServiceInterface
{
    function getAnnouncement();
    /**
     *
     * @param int $id
     * @return Announcement
     */
    function getAnnouncementById(int $id);
    /**
     *
     * @param array $announcement
     * @return void
     */
    function postAnnouncement(array $announcement);
    function putAnnouncement(array $announcement, int $id);
    function delAnnouncement(int $id);


    function restoreAnnouncement(int $id);
//   function getMegamenu();

}

