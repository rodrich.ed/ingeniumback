<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services\Interfaces;


interface ICategoriaServiceInterface
{
   function getCategoria();
   /**
    *
    * @param int $id
    * @return docentes
    */
   function getCategoriaById(int $id);
   /**
    *
    * @param array $categoria
    * @return void
    */
   function postCategoria(array $categoria);
   function putCategoria(array $categoria, int $id);
   function delCategoria(int $id);
   function putCategoriaId(string $field, string $value, string $id );

   function restoreDocentes(int $id);
//   function getMegamenu();

}
