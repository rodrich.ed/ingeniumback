<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Services\Interfaces;


interface ITipoServiceInterface
{
   function getTipo();
   /**
    * 
    * @param int $id
    * @return docentes
    */
   function getTipoById(int $id);
   /**
    * 
    * @param array $tipo
    * @return void
    */
   function postTipo(array $tipo);
   function putTipo(array $tipo, int $id);
   function delTipo(int $id);
   function putTipoId(string $field, string $value, string $id );
   
 
   function restoreTipo(int $id);
   

}