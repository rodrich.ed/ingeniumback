<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Resources;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class ResourcesController extends Controller
{
    private function getResources(Request $request) {
        try{
            // Orden por Creacion
            $query = Resources::orderBy('resources.'.$this->sort,$this->sortDirection);
            if($request->has('title'))
                $query->where('title','LiKE','%'.$request->title.'%');
            // Orden por Producto
            if($request->has('product_id'))
                $query->where('product_id', $request->product_id);
            if($request->has('category_id'))
               $query->whereHas('product.categoria_name', function($queryProd) use ($request) {
                   $queryProd->where('slug', $request->category_id);
            });
            return $query->with('product');
        }catch(Exception $e){
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    public function index(Request $request)
    {
        try {
            // Nueva Paginacion de Problemas
            $this->getPaginationParameters($request);
            $query = $this->getResources($request);
            return new JsonResponse($query->paginate($this->limit));
        } catch(Exception $e) {
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    public function create(Request $request)
    {
        try {
            $this->validate($request, $this->validators());
        } catch (Exception $e) {
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }

        $data = $this->getData($request);
        DB::beginTransaction();
        // Impedimento Transaccional
        try {
            $resource = Resources::create($data);
            DB::commit();
            return new JsonResponse($resource, 201);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    private function validators(){
        return [
            'title'=> 'required|max:300',
            'description'=> 'required',
            'link_img'=> 'required|max:300',
            'link_download'=> 'required|max:300',
            'product_id'=> 'required'
        ];
    }

    public function detail(Request $request, $idResource)
    {
        $blog = Resources::findOrFail($idResource);
        if ($blog) {
            return response()->json($blog);
        } else {
            return new JsonResponse([
                'message' => trans('Ocurrio un problema')
            ], 500);
        }
    }

    public function update(Request $request, $idResource)
    {
        try {
            $this->validate($request, array_merge($this->validators()));
        } catch (Exception $e) {
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('JSON MALFORMATION')], 500);
        }
        $data = $this->getData($request);
        DB::beginTransaction();

        try {
            $resource = Resources::findOrFail($idResource)->update($data);
            DB::commit();
            return new JsonResponse($resource, 200);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un error')], 500);
        }
    }

    public function destroy(Request $request, $idResource)
    {
        DB::beginTransaction();
        try {
            $resource = Resources::findOrFail($idResource);
            $resource->delete();
            DB::commit();
            return response()->json(['message'=>trans('Se elimino correctamente')]);
        }catch (ValidationException $e){
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    private function getData(Request $request, array $additional=[]){
        return $request->only(array_merge([
            'title','description', 'link_img','link_download',
            'product_id'], $additional));
    }
}
