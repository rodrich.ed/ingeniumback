<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Productos;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class BlogController extends Controller
{
    private function getBlogQuery(Request $request) {
        try{
            // Orden por Creacion
            $query = Blog::orderBy('blog.'.$this->sort,$this->sortDirection)->select('blog.title','blog.slug', 'blog.id', 'blog.short_description', 'blog.popularity', 'blog.interactions', 'blog.likes', 'blog.comments', 'blog.product_id', 'blog.image_web');
            if($request->has('title'))
                $query->where('title','LiKE','%'.$request->title.'%');
            // Orden por Producto
            if($request->has('product_id'))
                $query->where('product_id', $request->product_id);
            if($request->has('category_id'))
		$query->whereHas('product.categoria_name', function($queryProd) use ($request) {
                   $queryProd->where('slug', $request->category_id);
                });
            return $query->with(['product'=> function ($query) {
                $query->select('productos.id','productos.nombreproducto','productos.id_categoria', 'productos.id_tipo')
                ->with(['categoria' => function($query) {$query->select('categoria.nombre','categoria.id','categoria.slug');},
                        'tipo' => function($query) {$query->select('tipo.nombre', 'tipo.id');}]);
            }]);
        }catch(Exception $e){
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }
    public function getBlogRelated(Request $request){
        try{
           $query = Blog::all();

            return $query;
        }catch(Exception $e){
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }
    public function index(Request $request)
    {
        try {
            // Nueva Paginacion de Problemas
            $this->getPaginationParameters($request);
            $query = $this->getBlogQuery($request);
            return new JsonResponse($query->paginate($this->limit));
        } catch(Exception $e) {
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    public function create(Request $request)
    {
        // Funcion para Crear nuevos Blog
        try {
            $this->validate($request, $this->validators());
        } catch (Exception $e) {
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }

        $data = $this->getData($request);
        DB::beginTransaction();
        // Impedimento Transaccional
        try {
            $blog = Blog::create($data);
            DB::commit();
            return new JsonResponse($blog, 201);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    private function validators(){
        return [
            'title'=> 'required|max:200',
            'descriptions'=> 'required',
            'likes'=> 'required',
            'tags'=> 'required|max:300',
            'image_web'=> 'required|max:200',
            'interactions'=> 'required',
            'product_id'=> 'required'
        ];
    }

    public function detail(Request $request, $idBlog) {
        $blog = Blog::with(['commentsByBlog' => function($querys){
            $querys->where('validate', 1);
        },'teacher','product'=> function ($query) {
            $query->with(['categoria','tipo']);
        }])->find($idBlog);
        if ($blog) {
            return response()->json($blog);
        } else {
            return new JsonResponse([
                'message' => trans('Ocurrio un problema')
            ], 500);
        }
    }
    public function detailBySlug(Request $request, $slugBlog) {
        $blog = Blog::with(['commentsByBlog' => function($querys){
            $querys->where('validate', 1);
        },'teacher','product'=> function ($query) {
            $query->with(['categoria','tipo']);
        }])->where('slug',$slugBlog)->first();
        if ($blog) {
            return response()->json($blog);
        } else {
            return new JsonResponse([
                'message' => trans('Ocurrio un problema detailBySlug')
            ], 500);
        }
    }

    public function likes($idBlog)
    {
        DB::beginTransaction();
        try {
            $blog = Blog::where('id', $idBlog)->first();
            if ($blog == null) {
                return new JsonResponse(['message' =>'No se encontro blog con el ID'], 500);
            } else {
                $blog->likes = $blog->likes + 1;
                $blog->popularity = $blog->likes + $blog->interactions + $blog->comments;
                $blog->save();
                DB::commit();
                return new JsonResponse(['likes' =>$blog->likes, 'popularity' => $blog->popularity], 200);
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un error')], 500);
        }
    }

    public function interactionWeb($idBlog)
    {
        DB::beginTransaction();
        try {
            $blog = Blog::where('id', $idBlog)->first();
            if ($blog == null) {
                return new JsonResponse(['message' =>'No se encontro blog con el ID'], 500);
            } else {
                $blog->interactions = $blog->interactions + 1;
                $blog->popularity = $blog->likes + $blog->interactions + $blog->comments;
                $blog->save();
                DB::commit();
                return new JsonResponse(['interactions' =>$blog->interactions, 'popularity' => $blog->popularity], 200);
            }
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un error')], 500);
        }
    }

    public function update(Request $request, $idBlog)
    {
        try {
            $this->validate($request, array_merge($this->validators()));
        } catch (Exception $e) {
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('JSON MALFORMATION')], 500);
        }
        $data = $this->getData($request);
        DB::beginTransaction();

        try {
            $blog = Blog::findOrFail($idBlog)->update($data);
            DB::commit();
            return new JsonResponse($blog, 200);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un error')], 500);
        }
    }

    public function destroy(Request $request, $idBlog)
    {
        DB::beginTransaction();
        try {
            $blog = Blog::findOrFail($idBlog);
            $blog->delete();
            DB::commit();
            return response()->json(['message'=>trans('Se elimino correctamente')]);
        }catch (ValidationException $e){
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    private function getData(Request $request, array $additional=[]){
        return $request->only(array_merge([
            'title','descriptions', 'short_description','likes','tags',
            'image_web','interactions','popularity','comments','product_id','teacher_id'], $additional));
    }
}

