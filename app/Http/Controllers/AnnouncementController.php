<?php

namespace App\Http\Controllers;

use App\Services\Implementation\AnnouncementServiceImpl;
use App\Validator\AnnouncementValidator;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
class AnnouncementController extends Controller
{
    /**
     *
     * @var announcementServiceImpl
     */
    private $announcementService;
    /*
     * @var Request
     */
    private  $request;
    /*
     * @var ReclamacionesValidator
     */
    private $validator;

    public function __construct(AnnouncementServiceImpl $announcementService, Request $request, AnnouncementValidator $validator){
        $this->announcementService = $announcementService;
        $this->request = $request;
        $this->validator=$validator;
    }
    function getListAnnouncement(){
        return response($this->announcementService->getAnnouncement());
    }
    function createAnnouncement(){
        try{
		$this->announcementService->postAnnouncement($this->request->all());
		return new JsonResponse(['message' => trans('accept')],200);
	
	}catch(Exception $e){
		Log::error($e->getResponse());
		return new JsonResponse(['message'=>trans('Ocurrio un problema')],500);
		
	}
    }
    function deleteAnnouncement(int $id){
        $this->announcementService->delAnnouncement($id);
        return response("",204);
    }
    function updateAnnouncement($id){
        $response = response("",202);
        $this->announcementService->putAnnouncement($this->request->all(), $id);
        return $response;
    }
    function getByIdAnnouncement($id){
        return response($this->announcementService->getAnnouncementById($id));
    }


}
