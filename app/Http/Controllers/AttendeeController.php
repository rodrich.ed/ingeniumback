<?php

namespace App\Http\Controllers;

use App\Models\Attendeee;
use Illuminate\Http\Request;

class AttendeeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     *
    public function index(){
			try {
				$this->getPaginationParameters($request);
				$query = $this->getAttendeeQuery($request);
				return new JsonResponse($query->paginate($this->limit));
			} catch(Exception $e) {
				Log::error($e->getResponse());
				return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
			}
    }*/

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      try {
				$this->validate($request, $this->validators());
			} catch (Exception $e) {
				Log::error($e->getResponse());
				return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
			}
	
			$data = $this->getData($request);
			DB::beginTransaction();
	
			try {
				$attendee = Attendee::create($data);
				DB::commit();
				return new JsonResponse($attendee, 201);
			} catch (Exception $e) {
				DB::rollBack();
				Log::error($e->getResponse());
				return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
			}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Attendeee  $attendeee
     * @return \Illuminate\Http\Response
     *
    public function show(Attendeee $attendeee)
    {
        //
    }*/

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Attendeee  $attendeee
     * @return \Illuminate\Http\Response
     *
    public function update(Request $request, Attendeee $attendeee)
    {
      
    }*/

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Attendeee  $attendeee
     * @return \Illuminate\Http\Response
     *
    public function destroy(Attendeee $attendeee)
    {
      DB::beginTransaction();
			try {
				$attendee = Attendee::findOrFail($idAttendee);
				$attendee->delete();
				DB::commit();
				return response()->json(['message'=>trans('Se elimino correctamente')]);
			}catch (ValidationException $e){
				DB::rollBack();
				Log::error($e->getResponse());
				return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
			}
    }*/
		
		private function validators(){
			return [
				'attendee_id'=> 'required|integer',
				'first_name'=> 'required|max:200',
				'last_name'=> 'required|max:200',
				'phone_number'=> 'required|max:20',
				'email'=> 'required|max:100',
			];
		}
}
