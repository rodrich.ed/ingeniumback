<?php

namespace App\Http\Controllers;
use App\Services\Implementation\TipoServiceImpl;
use App\Validator\DocentesValidator;
use Illuminate\Http\Request;

class TipoController extends Controller
{
     /**
     *
     * @var tipoServiceImpl
     */
    private $tipoService;
    /*
     * @var Request
     */
    private  $request;
    /*
     * @var ReclamacionesValidator
     */
    private $validator;

    public function __construct(TipoServiceImpl $tipoService, Request $request, DocentesValidator $validator){
        $this->tipoService = $tipoService;
        $this->request = $request;
        $this->validator=$validator;
    }
    /*
     * @function funcion que permite consultar todos los tipo.
     */
    function getListTipo(){
        return response($this->tipoService->getTipo());
    }
    /*
     * @function funcion que permite crear un tipo recibe un json.
     */
    function createTipo(){
        $response = response("", 201);
        //echo '.' .$this->request;

        $validator = $this->validator->validate();
        if($validator->fails()){
            $response = response([
                "status" => 422,
                "message" => "Error",
                "errors" => $validator->errors()
            ],422);
        }else{
            $this->tipoService->postTipo($this->request->all());
        }

        return response($response);
    }
    /*
     * @function funcion que permite traer un tipo por id
     */
    function getListTipoId(int $id){
        return response($this->tipoService->getTipoById($id));
    }
    /*
     * @function funcion que permite actualizar los tipo
     */
     function putTipo(int $id){
        $response = response("",202);
        $this->tipoService->putTipo($this->request->all(), $id);
        return $response;
    }
    /*
     * @function funcion que permite eliminar un tipo
     */
    function deleteTipo(int $id){

        $this->tipoService->delTipo($id);
        return response("",204);
    }


}
