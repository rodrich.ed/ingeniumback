<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use App\Models\Productos;
use Illuminate\Support\Facades\Log;
use App\Models\Webinar;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class UtilsController extends Controller
{
    private  $request;
    public function __construct( Request $request){
        $this->request = $request;
    }
    function search(){
        $search=  $this->request['data'];
        $searchUp = ucfirst($search);
        $searchAllUp= strtoupper($search);
        $dataProductos =Productos::select('id','nombreproducto','slug','id_categoria')->with('categoria_name')->where('nombreproducto','LIKE','%'.$search.'%')->take(6)->get();
        $dataBlogs =Blog::select('id','title','slug','product_id')->with('area_name')->where('title','LIKE','%'.$search.'%')->take(6)->get();
$dataWebinar =Webinar::with('productInfo')->select('webinar_id','title','date','start_time','slug','product_id','recording_url')->where('title','LIKE','%'.$search.'%')
	->where(function($query) use ($search){
		$query->where(function($query){
			$query->where('date','>=',gmdate('Y-m-d',strtotime('-5 hours')));
		});
		$query->orWhere(function($query){
			$query->where('date','<',gmdate('Y-m-d',strtotime('-5 hours')));
			$query->whereNotNull('recording_url');
		});
	})->take(6)->get();

        for( $i=0;$i<count($dataBlogs);$i++){
            $dataBlogs[$i]['title']=str_replace( $search,'<strong>'.$search.'</strong>', $dataBlogs[$i]['title']);
            $dataBlogs[$i]['title']=str_replace( $searchUp,'<strong>'.$searchUp.'</strong>', $dataBlogs[$i]['title']);
            $dataBlogs[$i]['title']=str_replace( $searchAllUp,'<strong>'.$searchAllUp.'</strong>', $dataBlogs[$i]['title']);
        }
        for( $i=0;$i<count($dataProductos);$i++){
            $dataProductos[$i]['nombreproducto']=str_replace( $search,'<strong>'.$search.'</strong>', $dataProductos[$i]['nombreproducto']);
            $dataProductos[$i]['nombreproducto']=str_replace( $searchUp,'<strong>'.$searchUp.'</strong>', $dataProductos[$i]['nombreproducto']);
            $dataProductos[$i]['nombreproducto']=str_replace( $searchAllUp,'<strong>'.$searchAllUp.'</strong>', $dataProductos[$i]['nombreproducto']);
        }
        for( $i=0;$i<count($dataWebinar);$i++){
            $dataWebinar[$i]['title']=str_replace( $search,'<strong>'.$search.'</strong>', $dataWebinar[$i]['title']);
            $dataWebinar[$i]['title']=str_replace( $searchUp,'<strong>'.$searchUp.'</strong>', $dataWebinar[$i]['title']);
            $dataWebinar[$i]['title']=str_replace( $searchAllUp,'<strong>'.$searchAllUp.'</strong>', $dataWebinar[$i]['title']);
        }
				Log::info("mywebs: ".json_encode($dataWebinar));
        return json_encode(array('productos' =>$dataProductos,'blogs'=>$dataBlogs,'webinars'=>$dataWebinar ));
    }

    function createContactActiveCampaing()
    {
        try{
            // First we gotta search the contact on the  platform
            $url = "https://ingeniumefp.api-us1.com/admin/api.php?api_action=contact_view_email&api_key=47d7f49afbfbcead58b354044c6811d0e134982deb58b1ae3799211a178dfb22c1060ea6&api_output=json&email=".$this->request["email"];
            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $response =curl_exec($ch);
            $customer_search_result = json_decode($response);
            if(!empty($customer_search_result) && $customer_search_result->result_code == 1){
                // The customer exists
                $resultObj = $customer_search_result;
            }
            
            else{
                // Customer doesn't exist
                // Proceed to create the customer on the platform
                $body = array(
                    "email" => $this->request["email"],
                    "firstName" => $this->request["firstName"],
                    "lastName" => $this->request["lastName"],
                    "phone" => $this->request["phone"],
                );
                $url = "https://ingeniumefp.api-us1.com/api/3/contacts/sync";
                $params= json_encode(array('contact' =>$body));
                $response = $this->HTTPPost($url, $params);
                $resultObj = json_decode($response)->contact;
            }
            if($resultObj) {
                Log::info('myresp creatupd: '.json_encode($resultObj));
                if(isset($resultObj->errors)) {
                    return new JsonResponse(['message' => trans('3rd_party_error'), 'detail' => $resultObj->errors[0]], 500);
                } else {
                    
                    // Proceed to update the customer-list object
                    $list_customer_update_result = $this->assingContactToList($resultObj->id);
                    Log::info('myresp_listcust: '.json_encode($list_customer_update_result));
                    if($list_customer_update_result && $list_customer_update_result->status()==200){
                        // Proceed to update the customer-tag object
                        $tag_customer_update_result = $this->assingTagToContact($resultObj->id);
                        Log::info('myresp_tagcust: '.json_encode($tag_customer_update_result));
                        if($tag_customer_update_result && $tag_customer_update_result->status()==200)    return new JsonResponse(['message' => trans('Success'), 'detail'=> $resultObj], 200);
                        else return new JsonResponse(['message' => trans('Success'), 'detail'=> '3rd_party_incomplete'], 206);
                    }
                    else    return new JsonResponse(['message' => trans('Success'), 'detail'=> '3rd_party_incomplete'], 206);
                }
            }
        }catch (\Exception $e){
            return new JsonResponse(['message' => trans('Ocurrio un problema'),'error'=>$e->getMessage()], 500);
        }
    }
    function assingContactToList($contactid=null)
    {
        try{
            $body = array(
                "list" => $this->request["list"],
                "contact" => ($contactid ? $contactid : $this->request["contact"]),
                "status" => 1,
            );
            $url = "https://ingeniumefp.api-us1.com/api/3/contactLists";
            $params= json_encode(array('contactList' =>$body));
             $this->HTTPPost($url, $params);
            return new JsonResponse(['message' => trans('Success')], 200);
        }catch (\Exception $e){
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    function assingTagToContact($contact_id = null)
    {
        try {
             $arrayTags = $this->request->get('idtags');
                 for ($i = 0; $i < count($arrayTags); $i++) {
                      $body = array(
                          "contact" => ($contact_id ? $contact_id : $this->request->get('idContact')),
                          "tag" => $arrayTags[$i]
                      );
                      $url = "https://ingeniumefp.api-us1.com/api/3/contactTags";
                      $params = json_encode(array('contactTag' => $body));
                      $this->HTTPPost($url, $params);
                 }
            return new JsonResponse(['message' => trans('Success')], 200);

        }
        catch(Exception $e){
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }


    public static function HTTPPost($url, $params)
    {
        $additional_headers = array(
            'Content-Type: multipart/form-data',
            'Accept: application/json',
            'Api-Token: 47d7f49afbfbcead58b354044c6811d0e134982deb58b1ae3799211a178dfb22c1060ea6'
        );
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);
        $server_output = curl_exec($ch);
        return $server_output;
    }

}
