<?php



namespace App\Http\Controllers;



use App\Models\Webinar;

use App\Http\Resources\WebinarResource;

use App\Models\Productos;

use Illuminate\Http\Request;

use Illuminate\Http\JsonResponse;

use Illuminate\Validation\ValidationException;

use App\Services\Implementation\ActiveCampaignServiceImpl;

use Illuminate\Support\Facades\DB;

use Illuminate\Support\Facades\Mail;

use Illuminate\Support\Facades\URL;

use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Log;

//use App\Models\Attendee;



class WebinarController extends Controller

{

	private function getWebinarQuery(Request $request) {

		try {

			$query = Webinar::with('type_area_data')->orderBy($this->sort,$this->sortDirection);

			if($request->has('title'))

				$query->where('title','LiKE','%'.$request->title.'%');

			if($request->has('product_id'))

				$query->where('product_id', $request->product_id);

			if ($request->has('type')){

				$now_timestamp = strtotime('-5 hours');

				$date_arg = gmdate('Y-m-d',$now_timestamp);

				$time_arg = gmdate('H:i:s',$now_timestamp);



				switch ($request->type) {

					case 'today':		// for today view

						$query->where('date','=',$date_arg)->where('start_time','>',$time_arg)

						->whereNotNull('webinar_jam_id')

						->where('webinar_jam_id','<>',0)

						->whereNotNull('webinar_jam_schedule_id')

						->where('webinar_jam_schedule_id','<>',0)

						->whereNotNull('webinar_jam_account');

						break;



					case 'upcoming':		// for upcoming tab

						$query->whereDate('date','>',$date_arg)

						->whereNotNull('webinar_jam_id')

						->where('webinar_jam_id','<>',0)

						->whereNotNull('webinar_jam_schedule_id')

						->where('webinar_jam_schedule_id','<>',0)

						->whereNotNull('webinar_jam_account');

						break;



					case 'trending':		// for upcoming tab

						$query->whereDate('date','<',$date_arg)

						->whereNotNull('recording_url')

						->where('recording_url','!=','')

						->whereNotNull('active_campaign_area_tag_id')

						->whereNotNull('active_campaign_product_tag_id');

						break;

					

					case 'showcase':		// for home view, today + upcoming

						$query->where(function($query) use ($date_arg,$time_arg){

							$query->whereDate('date','=',$date_arg);

							$query->where('start_time','>',$time_arg);

							$query->whereNotNull('webinar_jam_id');

            				$query->where('webinar_jam_id','<>',0);

            				$query->whereNotNull('webinar_jam_schedule_id');

            				$query->where('webinar_jam_schedule_id','<>',0);

            				$query->whereNotNull('webinar_jam_account');

						})

						->orWhere(function($query) use ($date_arg){

							$query->whereDate('date','>',$date_arg);

							$query->whereNotNull('webinar_jam_id');

            				$query->where('webinar_jam_id','<>',0);

            				$query->whereNotNull('webinar_jam_schedule_id');

            				$query->where('webinar_jam_schedule_id','<>',0);

            				$query->whereNotNull('webinar_jam_account');

						})->orderBy('date','asc')->limit(12);

						break;



					default:

						http_response_code(400);

						die('Unknown "'.$request->type.'" webinar type');

						break;

				}

				Log::info("i've something ".json_encode($request->all()));

				if($this->ignore_id){

					Log::info("i've something to ignore indeed");

					$query->where('webinar_id', '!=',$this->ignore_id);

				}

			}

			return $query;

		} catch(Exception $e){

			Log::error($e->getResponse());

			return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);

		}

	}



	public function index(Request $request){

		/**IMPORTANT: types of webinars requested:

		 * - showcase: the ones that will be displayed on home view, limit = count(ingenium_academic_coordinators)

		 * - today: to be displayed on webinars list view, needs pagination

		 * - upcoming: to be displayed on webinars list view, needs pagination

		 * - trending: to be displayed on webinars list view, needs pagination

		 */

		try {

			$this->getPaginationParameters($request);

			$query = $this->getWebinarQuery($request);

			

			switch ($request->type) {

				case 'showcase':

					$resp = $query->get();

					Log::info('yapebatery');

					return WebinarResource::collection($resp);

					break;

				

				default:

					$paginated_result = $query->paginate($this->limit);

					WebinarResource::collection($paginated_result);

					Log::info('dafuk?: '.json_encode($paginated_result));

					return $paginated_result;

					break;

			}

		} catch(Exception $e) {

			Log::error($e->getResponse());

			return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);

		}

	}



	public function create(Request $request) {

		try {

			$this->validate($request, $this->validators());

		} catch (Exception $e) {

			Log::error($e->getResponse());

			return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);

		}



		$data = $this->getData($request);

		DB::beginTransaction();



		try {

			#Log::info('myreqwebinar: '.json_encode($data));

			// Upsert any webinar based on its title.

			$previousWebinarVersion = Webinar::where('title','=',$data['title'])->orWhere('slug','=',$data['slug']);

			#Log::info('myreqwebinarlength: '.json_encode($previousWebinarVersion->get()->isEmpty()));

			if($previousWebinarVersion->get()->isEmpty()) $previousWebinarVersion = Webinar::create($data);

			else $previousWebinarVersion->first()->fill($data)->save();

			//$webinar = Webinar::create($data);

			DB::commit();

			return new JsonResponse($previousWebinarVersion, 201);

		} catch (Exception $e) {

			DB::rollBack();

			Log::error($e->getResponse());

			return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);

		}

	}



	/**The CRM integration valitadors like validators are done

	 * in the CRM already so we do not need to use it again */

	private function attendee_validators(){

		return [

			'webinar_id'=> 'required|integer',

			'first_name'=> 'required|max:200',

			'last_name'=> 'required|max:200',

			'phone_number'=> 'required|max:20',

			'phone_country_code'=> 'required|max:6',

			'email'=> 'required|max:100',

		];

	}



	private function validators(){

		return [

			'title'=> 'required|max:200',

			'webinar_id'=> 'required|integer|max:999999999',

			'modality'=> 'required|max:200',

			'teacher_id'=> 'required|integer',#'exists:App\Models\Docentes,recordId',

			'date'=> 'required|date',

			'start_time'=> 'required|date_format:H:i:s',

			'end_time'=> 'required|date_format:H:i:s|after:start_time',

			'product_id'=> 'nullable|exists:App\Models\Productos,recordId',

			'atendees_limit'=> 'nullable|integer|max:100000',

			'thumbnail_url'=> 'nullable|max:200',

			'registration_url'=> 'nullable|max:200',

			'area'=>'nullable|max:100',

			'webinar_jam_account'=>'nullable|max:200',	// TODO: Since the webinar creation on Webinarjam and the CRM isn't

			'webinar_jam_id'=>'nullable|integer',				// automated yet, these fields won't be mandatory until we solve it

			'webinar_jam_schedule_id'=>'nullable|integer',

			'slug' => 'required|max:400'

		];

	}



	public function detail(Request $request) {

		#Log::debug('length: '.count($request->all()));

		if ($request->has('f80e4c')) $webinar = Webinar::with('teacherInfo')->where('slug',$request->f80e4c)->orWhere('webinar_id',$request->f80e4c);

		else{

			return new JsonResponse(['message' => 'invalid_id'], 500);

		}

		if ($webinar) {

			Log::info('myWebinar: '.json_encode($webinar->get()[0]));

			return new WebinarResource($webinar->get()[0]);

		} else {

			return new JsonResponse([

				'message' => trans('messages.conference_none')

			], 500);

		}

	}



	public function update(Request $request, $crmidWebinar) {

		$data = $this->getData($request);

		DB::beginTransaction();



		try {

			Log::info("searching for $crmidWebinar and ".URL::full());

			$webinar = Webinar::findOrFail($crmidWebinar)->update($data);

			DB::commit();

			return new JsonResponse($webinar, 200);

		} catch (Exception $e) {

			DB::rollBack();

			Log::error($e->getResponse());

			return new JsonResponse(['message' => trans('Ocurrio un error')], 500);

		}

	}



	/*

	 * servicio para actualizar un campo de un formulario del crmque es dinamico recibe

	 */

	function updateOneField(Request $request){

		$response = response("", 201);

		Webinar::findOrFail($request->recordId)->update(array($request->field =>$request->value));

		return $response;

	}



	public function destroy(Request $request, $idWebinar) {

		DB::beginTransaction();

		try {

			$webinar = Webinar::findOrFail($idWebinar);

			$webinar->delete();

			DB::commit();

			return response()->json(['message'=>trans('Se elimino correctamente')]);

		}catch (ValidationException $e){

			DB::rollBack();

			Log::error($e->getResponse());

			return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);

		}

	}



	private function getData(Request $request, array $additional=[]){

		return $request->only(array_merge(

			[

				'webinar_id',

				'crmid',

				'title',

				'modality',

				'teacher_id',

				'date',

				'start_time',

				'end_time',

				'product_id',

				'atendees_limit',

				'thumbnail_url',

				'registration_url',

				'webinar_jam_id',

				'webinar_jam_schedule_id',

				'webinar_jam_account',

				'slug'

			], $additional));

	}



	public function attendeeRegistration(Request $request){

		Log::info("myreee: ".json_encode($request->all()));

		try {

			$this->validate($request, $this->attendee_validators());

		} catch (Exception $e) {

			Log::error($e->getResponse());

			return new JsonResponse(['message' => trans('ATTENDEE_JSON_MALFORMED')], 500);

		}



		$data = $request->all();

		DB::beginTransaction();



		try {

			$webinar = Webinar::find($data['webinar_id']);

			if($webinar) {

				$url="https://api.webinarjam.com/webinarjam/register";

				$data['api_key']=WEBINAR_JAM_KEYS[$webinar['webinar_jam_account']];

				$data['webinar_id'] = $webinar['webinar_jam_id'];

				$data['schedule'] = $webinar['webinar_jam_schedule_id'];

				if(empty($data['webinar_id']) || empty($data['schedule']) || is_null($webinar['webinar_jam_account'])) return new JsonResponse(['message' => 'incomplete_webinar_jam_data'], 400);

				$data['phone'] = $data['phone_number'];

				$ch = curl_init ( $url );

				Log::debug("i'm sending this shit to the jam: ".json_encode($data));

				curl_setopt_array( $ch, array(CURLOPT_POST => 1,CURLOPT_POSTFIELDS => $data) );

				$result = curl_exec($ch);

				Log::debug("and my result is: ".json_encode($result));

				if(!$result){

					return new JsonResponse(['message' => '3rd_party_error'], 500);

				}

				$curlresponse = json_decode($result);

				//$curlresponse = str_replace('}1','',$result);

				$webinar->attendees_qty++;

				$webinar->save();

				DB::commit();



				// 2: Call the Active campaign service

				//$activeCampaignService = new ActiveCampaignServiceImpl();



				//return new JsonResponse({{, 200);

			}

			else return new JsonResponse(['message' => 'webinar_not_found'], 400);

		} catch (Exception $e) {

			DB::rollBack();

			Log::error($e->getResponse());

			return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);

		}

	}

}


