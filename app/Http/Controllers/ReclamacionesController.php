<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\Implementation\ReclamacionesServiceImpl;
use App\Validator\ReclamacionesValidator;

class ReclamacionesController extends Controller
{
    /**
     * 
     * @var ReclamacionesServiceImpl
     */
    private $reclamacionesService;
    /*
     * @var Request
     */
    private  $request;
    /*
     * @var ReclamacionesValidator
     */
    private $validator;
    
    public function __construct(ReclamacionesServiceImpl $reclamacionesService, Request $request, ReclamacionesValidator $validator){
        $this->reclamacionesService= $reclamacionesService;
        $this->request = $request;
        $this->validator=$validator;
    }
 
    function createReclamaciones(){
        $response = response("", 201);
        //echo '.' .$this->request;
        $dataJson="";
        $validator = $this->validator->validate();
        $id=-1;
        if($validator->fails()){
            $response = response([
                "status" => 422,
                "message" => "Error",
                "errors" => $validator->errors()
            ],422);
        }else{
            $id=$this->reclamacionesService->postReclamaciones($this->request->all()); 
            $dataJson=$this->reclamacionesService->getReclamacionesById($id);     
        }
        return response($dataJson[0]);
    }
    function getListReclamacionId(int $id){
        return response($this->reclamacionesService->getReclamacionesById($id));
    }
    
    function getListReclamaciones(){
        return response($this->reclamacionesService->getReclamaciones());
    }
    function putReclamaciones(int $id){
        $response = response("",202);
        $this->reclamacionesService->putReclamaciones($this->request->all(), $id);
        return $response;
    }
    function deleteReclamaciones(int $id){
        $this->reclamacionesService->delReclamaciones($id);
        return response("",204); 
    }
     function restoreReclamaciones(int $id){ 
        $this->reclamacionesService->restoreReclamaciones($id);
        return response("",204);
    }
     
}
