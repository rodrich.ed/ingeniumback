<?php

namespace App\Http\Controllers;
use App\Services\Implementation\CategoriaServiceImpl;
use App\Validator\DocentesValidator;
use Illuminate\Http\Request;

class CategoriaController extends Controller
{
     /**
     * 
     * @var DocentesServiceImpl
     */
    private $categoriaService;
    /*
     * @var Request
     */
    private  $request;
    /*
     * @var ReclamacionesValidator
     */
    private $validator;
    
    public function __construct(CategoriaServiceImpl $categoriaService, Request $request, DocentesValidator $validator){
        $this->categoriaService = $categoriaService;
        $this->request = $request;
        $this->validator=$validator;
    }
    /*
     * @function funcion que permite consultar todos los categoria.
     */
    function getListCategoria(){
        return response($this->categoriaService->getcategoria());
    }
    /*
     * @function funcion que permite crear un categoria recibe un json.
     */
    function createCategoria(){
        $response = response("", 201);
        //echo '.' .$this->request;
        
        $validator = $this->validator->validate();
        if($validator->fails()){
            $response = response([
                "status" => 422,
                "message" => "Error",
                "errors" => $validator->errors()
            ],422);
        }else{
            $this->categoriaService->postCategoria($this->request->all());       
        }
        
        return response($response);
    }
    /*
     * @function funcion que permite traer un categoria por id
     */
    function getListCategoriaId(int $id){
        return response($this->categoriaService->getCategoriaById($id));
    }
    /*
     * @function funcion que permite actualizar los categoria
     */
     function putCategoria(int $id){
        $response = response("",202);
        $this->categoriaService->putCategoria($this->request->all(), $id);
        return $response;
    }
    /*
     * @function funcion que permite eliminar un categoria
     */
    function deleteCategoria(int $id){
        
        $this->categoriaService->delCategoria($id);
        return response("",204); 
    } 
//    function consultarMegamenu(){
//        return $this->categoriaService->getMegamenu();
//        
//    }
 
}
