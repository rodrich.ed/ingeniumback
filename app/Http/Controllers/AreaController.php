<?php

namespace App\Http\Controllers;

use App\Services\Implementation\AreaServiceImpl;
use App\Validator\AreaValidator;
use App\Validator\DocentesValidator;
use Illuminate\Http\Request;

class AreaController extends Controller
{
    /**
     *
     * @var areaServiceImpl
     */
    private $areaService;
    /*
     * @var Request
     */
    private  $request;
    /*
     * @var ReclamacionesValidator
     */
    private $validator;

    public function __construct(AreaServiceImpl $areaService, Request $request, AreaValidator $validator){
        $this->areaService = $areaService;
        $this->request = $request;
        $this->validator=$validator;
    }
    function getListArea(){
        return response($this->areaService->getArea());
    }
    function createArea(){
        $response = response("", 201);
        $validator = $this->validator->validate();
        if($validator->fails()){
            $response = response([
                "status" => 422,
                "message" => "Error",
                "errors" => $validator->errors()
            ],422);
        }else{
            $this->areaService->postArea($this->request->all());
        }

        return response($response);
    }
    function deleteArea(int $id){
        $this->areaService->delArea($id);
        return response("",204);
    }
    function updateArea($id){
        $response = response("",202);
        $this->areaService->putArea($this->request->all(), $id);
        return $response;
    }

}
