<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Blog;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class CommentController extends Controller
{
    private function getComentsQuery(Request $request) {
        try{
            $query = Comment::orderBy($this->sort,$this->sortDirection);
            if($request->has('name'))
                $query->where('name','LiKE','%'.$request->name.'%');
             if($request->has('blog_id'))
                 $query->where('blog_id', $request->blog_id);
            return $query;
        } catch(Exception $e) {
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    public function index(Request $request)
    {
        try {
            $this->getPaginationParameters($request);
            $query = $this->getComentsQuery($request);
            return new JsonResponse($query->paginate($this->limit));
        } catch(Exception $e) {
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        try {
            $this->validate($request, $this->validators());
        } catch (Exception $e) {
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }

        $data = $this->getData($request);
        DB::beginTransaction();
        // Impedimento Transaccional
        try {
            $comment = Comment::create($data);
            $blog = Blog::where('id', $data['blog_id'])->first();
            $blog->comments = $blog->comments + 1;
            $blog->popularity = $blog->likes + $blog->interactions + $blog->comments;
            $blog->save();
            DB::commit();
            return new JsonResponse($comment, 201);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    private function validators(){
        return [
            'comment'=> 'required|max:300',
            'name'=> 'required',
            'email'=> 'required',
            'validate'=> 'required',
            'blog_id'=> 'required'
        ];
    }
    public function detail(Request $request, $idComment)
    {
        $blog = Comment::find($idComment);
        if ($blog) {
            return response()->json($blog);
        } else {
            return new JsonResponse([
                'message' => trans('Ocurrio un problema')
            ], 500);
        }
    }

    public function update(Request $request, $idComment)
    {
        try {
            $this->validate($request, array_merge($this->validators()));
        } catch (Exception $e) {
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('JSON MALFORMED')], 500);
        }
        $data = $this->getData($request);
        DB::beginTransaction();

        try {
            $blog = Comment::findOrFail($idComment)->update($data);
            DB::commit();
            return new JsonResponse($blog, 200);
        } catch (Exception $e) {
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un error')], 500);
        }
    }

    public function destroy(Request $request, $idComment)
    {
        DB::beginTransaction();
        try {
            $blog = Comment::findOrFail($idComment);
            $blog->delete();
            DB::commit();
            return response()->json(['message'=>trans('Se elimino correctamente')]);
        }catch (ValidationException $e){
            DB::rollBack();
            Log::error($e->getResponse());
            return new JsonResponse(['message' => trans('Ocurrio un problema')], 500);
        }
    }

    private function getData(Request $request, array $additional=[]){
        return $request->only(array_merge(['comment','name','email','validate','blog_id'], $additional));
    }
}
