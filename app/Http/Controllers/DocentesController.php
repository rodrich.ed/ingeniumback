<?php

namespace App\Http\Controllers;
use App\Services\Implementation\DocentesServiceImpl;
use App\Validator\DocentesValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class DocentesController extends Controller
{
     /**
     * 
     * @var DocentesServiceImpl
     */
    private $docentesService;
    /*
     * @var Request
     */
    private  $request;
    /*
     * @var ReclamacionesValidator
     */
    private $validator;
    
    public function __construct(DocentesServiceImpl $docentesService, Request $request, DocentesValidator $validator){
        $this->docentesService = $docentesService;
        $this->request = $request;
        $this->validator=$validator;
    }
    /*
     * @function funcion que permite consultar todos los docentes.
     */
    function getListDocentes(){
        return response($this->docentesService->getDocentes());
    }
    /*
     * @function funcion que permite crear un docente recibe un json.
     */
    function createDocentes(){
        $response = response("", 201);
        //echo '.' .$this->request;
        
        $validator = $this->validator->validate();
        if($validator->fails()){
            $response = response([
                "status" => 422,
                "message" => "Error",
                "errors" => $validator->errors()
            ],422);
        }else{
            $this->docentesService->postDocentes($this->request->all());       
        }
        
        return response($response);
    }
    /*
     * @function funcion que permite traer un docentes por id
     */
    function getListDocentesId(int $id){
        return response($this->docentesService->getDocentesById($id));
    }
    /*
     * @function funcion que permite actualizar los docentes
     */
     function putDocentes(int $id){
        $response = response("",202);
        $this->docentesService->putDocentes($this->request->all(), $id);
        return $response;
    }
    /*
     * @function funcion que permite eliminar un producto
     */
    function deleteDocentes(int $id){
        
        $this->docentesService->delDocentes($id);
        return response("",204); 
    } 
    /*
     * servicio para actualizar un campo de un formulario del crmque es dinamico recibe
     */
    function updateOneFieldDocente(){
        $response = response("", 201);
        //echo '.' .$this->request;
        
        $validator = $this->validator->validate();
        if($validator->fails()){
            $response = response([
                "status" => 422,
                "message" => "Error",
                "errors" => $validator->errors()
            ],422);
        }else{
            $this->docentesService->putDocentesId($this->request['field'], $this->request['value'], $this->request['recordId']);  
            //echo $this->request;
            //echo $this->request ;
            //echo $this->request['field'].'-'.$this->request['value'].'-'.$this->request['recordId'];
        }
        return $response;
    }
    
}
