<?php



namespace App\Http\Controllers;



use Illuminate\Http\JsonResponse;

use Illuminate\Http\Request;

use App\Services\Implementation\ProductosServiceImpl;

use App\Validator\ProductosValidator;

use Illuminate\Support\Facades\Log;



class ProductosController extends Controller

{

     /**

     *

     * @var ReclamacionesServiceImpl

     */

    private $productoService;

    /*

     * @var Request

     */

    private  $request;

    /*

     * @var ReclamacionesValidator

     */

    private $validator;



    public function __construct(ProductosServiceImpl $productoService, Request $request, ProductosValidator $validator){

        $this->productoService= $productoService;

        $this->request = $request;

        $this->validator=$validator;

    }

    /*

     * @function funcion que permite consultar todos los productos.

     */

    function getListProductos(Request $request){

			$this->getPaginationParameters($request);

			$query = $this->productoService->getProductos($request);

			$paginatedResult = new JsonResponse($query->paginate($this->limit));

			$filteredProducts = array();

			return $paginatedResult;

    }

    /*

     * @function funcion que permite crear un producto recibe un json.

     */

    function createProductos(){

			$response = response("", 201);

			try {

				$this->validate($this->request, $this->validators());

			} catch (Exception $e) {

				Log::error($e->getResponse());

				return new JsonResponse(['status'=>'error','message' => 'Ocurrio un problema'], 500);

			}

			

			$response = $this->productoService->postProductos($this->request->all());

			return new JsonResponse(['status' => 'success'], 201);

    }

    /*

     * @function funcion que permite traer un producto por id

     */

    function getListProductosId(string $id){

        return response($this->productoService->getProductosById($id));

    }

    /*

     * @function funcion que permite actualizar los productos

     */

     function putProductos(int $id){

			$response = response("",202);

			$this->productoService->putProductos($this->request->all(), $id);

			return $response;

    }

    /*

     * @function funcion que permite eliminar un producto

     */

    function deleteProductos(int $id){

			$this->productoService->delProductos($id);

			return response("",204);

    }
	public function messages()
	{
		return [
			'fechainicio.after' => 'La fecha de fin debe ser mayor a la fecha de inicio', ];
	}



		private function validators(){

			return [

				"nombreproducto"=>"required",

				"partnumber"=>"required",

				"id_categoria"=>"required",

				"id_tipo"=>"required",

				"preciounitario"=>"required",

				"duracion"=>"required",

				"estructuracurricular"=>"required",

				"fechainicio"=>"required",

				"fechafin"=>"nullable",

				"diaclases1"=>"required",

				"diaclases2"=>"nullable",

				"horainicio1"=>"required",

				"horainicio2"=>"nullable",

				"horafin1"=>"required|after:horainicio1",

				"horafin2"=>"nullable",

				"queaprendere"=>"required",

				"dirigidoa"=>"required",

				"certificacion"=>"required",

				"certificacion2"=>"nullable",

				"link_imagen"=>"nulllable",

				"link_video"=>"nulllable",

				"aperturado"=>"required",

				"id_docente1"=>"required",

				"id_docente2"=>"nullable",

				"id_docente3"=>"nullable",

				"id_docente4"=>"nullable",

				"id_docente5"=>"nullable",

				"id_docente6"=>"nullable",

				"id_docente7"=>"nullable",

				"id_docente8"=>"nullable",

				"id_docente9"=>"nullable",

				"id_docente10"=>"nullable",

				"enrollmentprice"=>"required",

				"maxfees"=>"required",

			];

		}

    /*

     * servicio para actualizar un campo de un formulario del crmque es dinamico recibe

     */

    function updateOneFieldProduct(){

			$response = response("", 201);

			//echo '.' .$this->request;



			$validator = $this->validator->validate();

			if($validator->fails()){

				$response = response([

						"status" => 422,

						"message" => "Error",

						"errors" => $validator->errors()

				],422);

			}else{

				$this->productoService->putProductosId($this->request['field'], $this->request['value'], $this->request['recordId']);

				echo $this->request['field'].'-'.$this->request['value'].'-'.$this->request['recordId'];

			}

			return $response;

    }



    function buscarProductosByCategoria(){

    	return $this->productoService->getProductosByCategoria($this->request->all());

    }

    function consultarMegamenu(){

			return $this->productoService->getMegamenu();

    }

    function consultarRecientes($id){

			$new_id = explode(',',$id);

			$area_id = $new_id[0];

			$product_id = (sizeof($new_id) > 1?$new_id[1]:0);

			return $this->productoService->getRecientes($area_id,$product_id);

    }



    function testServiceWebinar(){

			$params=array(

				"api_key"=>$this->request->get('api_key'),

				"webinar_id"=>$this->request->get('webinar_id'),

				"first_name" =>$this->request->get('first_name'),

				"last_name"=>$this->request->get('last_name'),

				"email"=>$this->request->get('email'),

				"schedule"=>$this->request->get('schedule'),

				"phone_country_code"=>$this->request->get('phone_country_code'),

				"phone"=>$this->request->get('phone')

			);

			$url = "https://api.webinarjam.com/webinarjam/register";

			return $this->HTTPPost($url, $params);

    }

		public static function HTTPPost($url, $params) {

			$additional_headers = array(

					'Content-Type: multipart/form-data'

			);

			$ch = curl_init($url);

			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");

			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);

			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			curl_setopt($ch, CURLOPT_HTTPHEADER, $additional_headers);

			$server_output = curl_exec($ch);

			return $server_output;

    }

    

	function consultarProductosIdCategoria(string $id){
		return $this->productoService->findProductsById($id);
	}



}


