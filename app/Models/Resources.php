<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Resources extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory, \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table ='resources';
    protected $fillable = [
        'id',
        'title',
        'description',
        'link_img',
        'link_download',
        'product_id',
        'created_at',
        'updated_at'
    ];

    public function product(){
        // Nueva Relacion de Productos
        return $this->hasOne(Productos::class, 'id', 'product_id');
    }
	public function area(){
		return $this->belongsTo(Productos::class, 'product_id','id')->with('categoria_name');
	}
}
