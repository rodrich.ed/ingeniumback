<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Lumen\Auth\Authorizable;

class Categoria extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory, \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table ='categoria';
    protected $fillable = [
        'nombre',
        'colorprimario',
        'colorsecundario',
        'image_code',
        'created_at',
        'updated_at'
    ];

//    public function productos(){
//        //return $this->hasMany(Productos::class,'id_categoria','id')->select(['id','nombreproducto','id_categoria','id_tipo']);
//
//        return $this->hasManyThrough(Productos::class,Tipo::class,'id','id_tipo','id','id');
//    }
}
