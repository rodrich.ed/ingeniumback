<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Lumen\Auth\Authorizable;

class Blog extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory, \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table ='blog';
    protected $fillable = [
        'title',
        'descriptions',
        'short_description',
        'likes',
        'tags',
        'image_web',
        'interactions',
        'popularity','comments','product_id','teacher_id',
        'created_at',
        'updated_at'
    ];

    public function product(){
        // Nueva Relacion de Productos
        return $this->hasOne(Productos::class, 'id', 'product_id');
    }
    public function area_name(){
        // Nueva Relacion de Productos
        return $this->hasOne(Productos::class, 'id', 'product_id')->select('id','id_categoria')->with('categoria_name');
    }
    public function teacher(){
        // Nueva Relacion de Productos
        return $this->hasOne(Docentes::class, 'recordId', 'teacher_id');
    }
    public function commentsByBlog(){
        // Nueva Relacion de Productos
        return $this->hasMany(Comment::class, 'blog_id');
    }
}
