<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Libroreclamaciones extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory, \Illuminate\Database\Eloquent\SoftDeletes;

    /**
     * atributos de la tabla.
     *
     * @var array
     */
    protected $table ='libroreclamaciones';
    protected $fillable = [
        'razonsocial',
        'domicilio',
        'fecha',
        'documento',
        'telefono',
        'email',
        'tipo',
        'monto',
        'descripcion',
        'motivo',
        'detalle',
        'pedido',
        'sucursal',
        'estado',
        'mode',
        'created_at',
        'updated_at'  
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

}
