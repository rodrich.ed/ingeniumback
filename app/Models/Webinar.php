<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Lumen\Auth\Authorizable;
use App\Models\Productos;

class Webinar extends Model implements AuthenticatableContract, AuthorizableContract
{
	use Authenticatable, Authorizable, HasFactory, \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table ='webinars';
	protected $primaryKey = 'webinar_id';
	protected $dates = ['date','start_time','end_time'];
	protected $casts = ['start_time' => 'datetime:H:i:s','date'=>'datetime:Y-m-d'];
	protected $fillable = [
		'crmid',
		'webinar_id',
		'title',
		'modality',
		'teacher_id',
		'date',
		'start_time',
		'end_time',
		'product_id',
		'atendees_limit',
		'thumbnail_url',
		'registration_url',
		'recording_url',
		'webinar_jam_id',
		'webinar_jam_schedule_id',
		'webinar_jam_account',
		'active_campaign_area_tag_id',
		'active_campaign_product_tag_id',
		'area',
		'slug',
		'created_at',
		'updated_at'
	];

	public function type_area_data(){
		$my_product = $this->belongsTo(Productos::class, 'product_id','recordId')->select('id','id_categoria','id_tipo','nombreproducto','link_imagen','slug')->with('categoria_name','tipo');
		return $my_product;
	}

	public function teacherInfo(){
		return $this->hasOne(Docentes::class,'recordId','teacher_id');
	}

	public function productInfo(){
		$my_product = $this->belongsTo(Productos::class, 'product_id','recordId')->select(array('recordId','id','id_categoria','nombreproducto','link_imagen','slug'))->with('categoria_name');
		return $my_product;
	}
}
