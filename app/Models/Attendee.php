<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Lumen\Auth\Authorizable;

class Attendee extends Model implements AuthenticatableContract, AuthorizableContract
{
	use Authenticatable, Authorizable, HasFactory, \Illuminate\Database\Eloquent\SoftDeletes;
	protected $table ='webinar_attendee';
	protected $primary_key = 'attendee_id';
	protected $fillable = [
		'webinar_id',
		'first_name',
		'last_name',
		'phone_number',
		'email',
		'created_at',
		'updated_at'
	];
}
