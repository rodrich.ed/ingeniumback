<?php



namespace App\Models;



use Illuminate\Auth\Authenticatable;

use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

use Illuminate\Database\Eloquent\Factories\HasFactory;

use Illuminate\Database\Eloquent\Model;

use Laravel\Lumen\Auth\Authorizable;

use App\Models\Docentes;

use App\Models\Tipo;

use App\Models\Categoria;

use Illuminate\Support\Facades\Log;

class Productos extends Model implements AuthenticatableContract, AuthorizableContract

{

	use Authenticatable, Authorizable, HasFactory, \Illuminate\Database\Eloquent\SoftDeletes;

	protected $table ='productos';

	protected $dates = ['fechainicio','fechafin'];

	protected $casts = ['fechainicio' => 'datetime:Y-m-d','fechafin'=>'datetime:Y-m-d'];

	protected $fillable = [

		'id',

		'recordId',

		'nombreproducto',

		'partnumber',

		'id_categoria',

		'id_tipo',

		'preciounitario',

		'duracion',

		'estructuracurricular',

		'fechainicio',

		'fechafin',

		'diaclases1',

		'diaclases2',

		'horainicio1',

		'horainicio2',

		'horafin1',

		'horafin2',

		'queaprendere',

		'dirigidoa',

		'certificacion',

		'certificacion2',

		'link_imagen',

		'link_video',

		'webinar1_id',

		'webinar2_id',

		'webinar3_id',

		'webinar4_id',

		'webinar5_id',

		'id_docente1',

		'id_docente2',

		'id_docente3',

		'id_docente4',

		'id_docente5',

		'id_docente6',

		'id_docente7',

		'id_docente8',

		'id_docente9',

		'id_docente10',

		'aperturado',

		'enrollmentprice',

		'maxfees',

		'active_campaign_area_tag_id',

		'active_campaign_product_tag_id',

		'slug',

		'created_at',

		'updated_at' 

	];



	

	public function docente1(){

		return $this->hasMany(Docentes::class,'recordId','id_docente1');

	}

	public function docente2(){

		return $this->hasMany(Docentes::class,'recordId','id_docente2');

	}

	public function docente3(){

		return $this->hasMany(Docentes::class,'recordId','id_docente3');

	}

	public function docente4(){

		return $this->hasMany(Docentes::class,'recordId','id_docente4');

	}

	public function docente5(){

		return $this->hasMany(Docentes::class,'recordId','id_docente5');

	}

	public function docente6(){

		return $this->hasMany(Docentes::class,'recordId','id_docente6');

	}

	public function docente7(){

		return $this->hasMany(Docentes::class,'recordId','id_docente7');

	}

	public function docente8(){

		return $this->hasMany(Docentes::class,'recordId','id_docente8');

	}

	public function docente9(){

		return $this->hasMany(Docentes::class,'recordId','id_docente9');

	}

	public function docente10(){

		return $this->hasMany(Docentes::class,'recordId','id_docente10');

	}

	public function categoria(){

		return $this->belongsTo(Categoria::class,'id_categoria','id');

	}

	public function categoria_name(){

		return $this->belongsTo(Categoria::class,'id_categoria','id')->select(array('id','nombre','slug'));

	}

	public function tipo(){

		return $this->hasOne(Tipo::class,'id','id_tipo');

	}

	public function setFechafinAttribute($value){

		$this->attributes['fechafin'] = (strlen($value)<4 ? null :$value);

	}

	public function setHorainicio2Attribute($value){

		$this->attributes['horainicio2'] = (strlen($value)<4 ? null :$value);

	}

	public function setHorafin2Attribute($value){

		$this->attributes['horafin2'] = (strlen($value)<4 ? null : $value);

	}

	public function setDiaclases2Attribute($value){

		$this->attributes['diaclases2'] = (strlen($value)<4 ? null : $value);

	}

	public function setLinkVideoAttribute($value){

		$this->attributes['link_video'] = (strlen($value)<4 ? null : $value);

	}

}


