<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Comment extends  Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory, \Illuminate\Database\Eloquent\SoftDeletes;
    protected $table ='comment';
    protected $fillable = [
        'comment',
        'name',
        'email',
        'validate',
        'blog_id',
        'created_at',
        'updated_at'
    ];
    public function blog(){
        // Nueva Relacion de Blog
        return $this->hasOne(Blog::class, 'id');
    }

}
