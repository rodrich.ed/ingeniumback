<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Lumen\Auth\Authorizable;
use App\Models\Area;
class Announcement extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory, \Illuminate\Database\Eloquent\SoftDeletes;

    protected $table ='announcement';
    protected $fillable = [
        'id_area',
        'title',
        'descriptions',
        'requires',
        'link',
        'quantity',
        'place',
        'schedule',
        'state',
        'created_at',
        'updated_at'
    ];
    public function area(){
        return $this->hasOne(Area::class,'id','id_area');
    }

}
