<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(["prefix" => "/v1"], function () use ($router)
{

    $router->group(["prefix" => "/reclamaciones"], function () use ($router)
    {
        $router->post('/regreclamaciones','ReclamacionesController@createReclamaciones');
        $router->get('/getreclamaciones','ReclamacionesController@getListReclamaciones');
        $router->put("/{id}",'ReclamacionesController@putReclamaciones');
        $router->delete("/{id}",'ReclamacionesController@deleteReclamaciones');
        $router->get("/{id}/restore",'ReclamacionesController@restoreReclamaciones');
        $router->get("/pdf", 'ReclamacionesController@mostrarPDF');
    });

    $router->group(["prefix" => "/docentes"], function () use ($router)
    {
        $router->get('/getdocentes','DocentesController@getListDocentes');
        $router->post('/regdocente','DocentesController@createDocentes');
        $router->put("/{id}",'DocentesController@putDocentes');
        /*$router->delete("/{id}",'DocentesController@deleteDocentes');*/
        $router->post('/updateone','DocentesController@updateOneFieldDocente');
    });

    $router->group(["prefix" => "/tipo"], function () use ($router)
    {
        $router->get('/gettipo','TipoController@getListTipo');
        $router->post('/regtipo','TipoController@createTipo');
        $router->put("/{id}",'TipoController@putTipo');
        $router->delete("/{id}",'TipoController@deleteTipo');
    });

    $router->group(["prefix" => "/categoria"], function () use ($router)
    {
        $router->get('/getcategoria','CategoriaController@getListCategoria');
        $router->post('/regcategoria','CategoriaController@createCategoria');
        $router->put("/{id}",'CategoriaController@putCategoria');
        $router->delete("/{id}",'CategoriaController@deleteCategoria');
//        $router->get('/getmegamenu','CategoriaController@consultarMegamenu');

    });

    $router->group(["prefix" => "/productos"], function () use ($router)
    {
        $router->post('/regproducto','ProductosController@createProductos');
        $router->put("/{id}",'ProductosController@putProductos');
        /*$router->delete("/{id}",'ProductosController@deleteProductos');*/
        $router->post('/updateone','ProductosController@updateOneFieldProduct');
        $router->get("/findproductid/{id}",'ProductosController@getListProductosId');
        $router->post("/categoriasproducto",'ProductosController@buscarProductosByCategoria');
        $router->get('/getmegamenu','ProductosController@consultarMegamenu');
        $router->get('/getproductos','ProductosController@getListProductos');
        $router->get('/getrecientes/{id}','ProductosController@consultarRecientes');
        $router->get('/getproductoscategoria/{id}','ProductosController@consultarProductosIdCategoria');
        $router->post('/testwebinar','ProductosController@testServiceWebinar');
    });

    $router->group(["prefix" => "/blog"], function () use ($router)
    {
   $router->put('/{idBlog}', 'BlogController@update');

        $router->get('', 'BlogController@index');
                        $router->get('/related', 'BlogController@getBlogRelated');

        $router->get('/{idBlog}', 'BlogController@detail');
	$router->get('/bySlug/{slugBlog}', 'BlogController@detailBySlug');
        $router->post('', 'BlogController@create');
        $router->delete('/{idBlog}', 'BlogController@destroy');
        $router->get('/like/{idBlog}', 'BlogController@likes');
        $router->get('/interact/{idBlog}', 'BlogController@interactionWeb');
    });

    $router->group(["prefix" => "/comment"], function () use ($router)
    {
        $router->get('', 'CommentController@index');
        $router->get('/{idComment}', 'CommentController@detail');
        $router->post('', 'CommentController@create');
        $router->put('/{idComment}', 'CommentController@update');
        $router->delete('/{idComment}', 'CommentController@destroy');
    });

    $router->group(["prefix" => "/area"], function () use ($router)
    {
        $router->get('getArea', 'AreaController@getListArea');
        $router->post('createArea', 'AreaController@createArea');
        $router->delete('/delete/{id}', 'AreaController@deleteArea');
        $router->put('/update/{id}', 'AreaController@updateArea');

    });
    $router->group(["prefix" => "/resources"], function () use ($router)
    {
        $router->get('', 'ResourcesController@index');
        $router->post('', 'ResourcesController@create');
        $router->post('', 'ResourcesController@create');
        $router->get('/{idResource}', 'ResourcesController@detail');
        $router->put('/{idResource}', 'ResourcesController@update');
        $router->delete('/{idResource}', 'ResourcesController@destroy');
    });
    $router->group(["prefix" => "/announcement"], function () use ($router)
    {
        $router->get('getAnnouncement', 'AnnouncementController@getListAnnouncement');
        $router->post('createAnnouncement', 'AnnouncementController@createAnnouncement');
        $router->delete('/delete/{id}', 'AnnouncementController@deleteAnnouncement');
        $router->put('/update/{id}', 'AnnouncementController@updateAnnouncement');
        $router->get("/find/{id}",'AnnouncementController@getByIdAnnouncement');
    });
	$router->group(["prefix" => "/webinars"], function () use ($router)
    {
		$router->get('', 'WebinarController@index');
        $router->get('/detail', 'WebinarController@detail');
        $router->post('/regwebinar', 'WebinarController@create');
		$router->post('/updateone','WebinarController@updateOneField');
        $router->put('/{crmidWebinar}', 'WebinarController@update');
        $router->delete('/{idWebinar}', 'WebinarController@destroy');
        $router->post('/registration', 'WebinarController@attendeeRegistration');
    });
    $router->group(["prefix" => "/utils"], function () use ($router)
    {
        $router->post('search', 'UtilsController@search');
        $router->post('createContactAc','UtilsController@createContactActiveCampaing');
        $router->post('assingContactToList','UtilsController@assingContactToList');
        $router->post('assingTagToContact','UtilsController@assingTagToContact');
    });

    $router->group(["prefix" => "/resources"], function () use ($router)
    {
        $router->get('', 'ResourcesController@index');
        $router->post('', 'ResourcesController@create');
        $router->get('/{idResource}', 'ResourcesController@detail');
        $router->put('/{idResource}', 'ResourcesController@update');
        $router->delete('/{idResource}', 'ResourcesController@destroy');
    });

});
