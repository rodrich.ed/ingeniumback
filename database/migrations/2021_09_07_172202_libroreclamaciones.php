<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Libroreclamaciones extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('libroreclamaciones', function (Blueprint $table){
            $table->engine ='InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('razonsocial',255);
            $table->string('domicilio',300);
            $table->date('fecha');
            $table->string('documento',20);
            $table->string('telefono',15);
            $table->string('email',100);
            $table->string('tipo',20);
            $table->string('monto',30);
            $table->string('descripcion',255);
            $table->string('motivo',50);
            $table->string('detalle',2000);
            $table->string('pedido',2000);
            $table->string('sucursal',50);
            $table->string('estado',1);
            $table->string('mode',1);
            $table->timestamps();
            
            $table->softDeletes();
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
