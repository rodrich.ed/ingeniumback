<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Webinars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webinars', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('webinar_id')->unsigned();
//            $table->unsignedInteger('crmid');
            $table->string('title', 300);
            $table->string('modality', 300);
            $table->date('date');
            $table->time('start_time');
            $table->time('end_time');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('teacher_id');
            $table->unsignedInteger('attendees_qty')->default('0');	// increased each time someone registers for the webinar
            $table->string('thumbnail_url', 300);										// should be set by mkt, but the default area thumbnail can be used (to be approved)
            $table->string('registration_url', 300)->nullable();		// marketing set this value
            $table->string('recording_url', 300)->nullable();				// also marketing
            $table->string('webinar_jam_id', 300)->nullable();			// can be retrieved from the webinar jam API, must be done by a human temporary
            $table->string('webinar_jam_schedule_id', 300)->nullable();			// can be retrieved from the webinar jam API, must be done by a human temporary
            $table->tinyInteger('webinar_jam_account')->nullable();			// must be done by a human always (0 for marketing@ingenium.edu.pe, 1 for webinar@ingenium.edu.pe)
            $table->unsignedBigInteger('active_campaign_area_tag_id')->nullable();				// as its name indicates, will be filled at the creation time
            $table->unsignedBigInteger('active_campaign_product_tag_id')->nullable();		// as its name indicates, will be filled at the creation time
            $table->string('area');			// we will retrieve this from crm at the creation of a new record
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('product_id')->references('id')->on('productos');
            $table->foreign('teacher_id')->references('recordId')->on('docentes');
        });
    }
		// NOTE: if some of the external fields like act_campaign_area, area are erronous we will simply update it manually

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
