<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Productos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {       Schema::create('docentes', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('recordId')->unsigned();
                $table->string('nombre', 255);
                $table->string('apellido', 255);
                $table->string('grado', 50);
                $table->string('cargoactual', 255);
                $table->string('experiencia', 255);
                $table->string('foto', 200);
                $table->timestamps();
                $table->softDeletes();
            });
              Schema::create('categoria', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id')->unsigned();
                $table->string('nombre', 255);
                $table->string('colorprimario',100);
                $table->string('colorsecundario',100);
                $table->string('image_code',100);
                $table->timestamps();
                $table->softDeletes();

            });
            Schema::create('tipo', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->bigIncrements('id')->unsigned();
                $table->string('nombre', 255);
                $table->string('abreviatura', 255);
                 $table->string('nombremegamenu',100);
                $table->timestamps();
                $table->softDeletes();

            });
            Schema::create('productos', function (Blueprint $table){
                $table->engine ='InnoDB';
                $table->bigIncrements('id')->unsigned();
                $table->integer('recordId');
                $table->string('nombreproducto',255);
                $table->string('partnumber',255);
                $table->unsignedBigInteger('id_categoria')->nullable();
                $table->unsignedBigInteger('id_tipo')->nullable();
                $table->string('preciounitario',255);
                $table->string('duracion',100)->nullable();
                $table->longText('estructuracurricular');
                $table->date('fechainicio',20);
                $table->date('fechafin',20)->nullable();
                $table->string('diaclases1',15)->nullable();
                $table->string('diaclases2',15)->nullable();
                $table->string('horainicio1',20)->nullable();
                $table->string('horainicio2',20)->nullable();
                $table->string('horafin1',255)->nullable();
                $table->string('horafin2',50)->nullable();
                $table->longText('queaprendere',2000)->nullable();
                $table->longText('dirigidoa',2000)->nullable();
                $table->string('certificacion',2000)->nullable();
                $table->string('link_imagen',100)->nullable();
                $table->string('link_video',100)->nullable();
//                $table->unsignedBigInteger('webinar1_id')->nullable();
//                $table->unsignedBigInteger('webinar2_id')->nullable();
//                $table->unsignedBigInteger('webinar3_id')->nullable();
//                $table->unsignedBigInteger('webinar4_id')->nullable();
//                $table->unsignedBigInteger('webinar5_id')->nullable();
                $table->unsignedBigInteger('id_docente1')->nullable();
                $table->unsignedBigInteger('id_docente2')->nullable();
                $table->unsignedBigInteger('id_docente3')->nullable();
                $table->unsignedBigInteger('id_docente4')->nullable();
                $table->unsignedBigInteger('id_docente5')->nullable();
                $table->unsignedBigInteger('id_docente6')->nullable();
                $table->unsignedBigInteger('id_docente7')->nullable();
                $table->unsignedBigInteger('id_docente8')->nullable();
                $table->unsignedBigInteger('id_docente9')->nullable();
                $table->unsignedBigInteger('id_docente10')->nullable();
                $table->string('aperturado',5)->nullable();
                $table->string('enrollmentprice',10)->nullable();
                $table->string('maxfees',10)->nullable();

                $table->timestamps();
                $table->softDeletes();

                $table->foreign('id_docente1')->references('recordId')->on('docentes');
                $table->foreign('id_docente2')->references('recordId')->on('docentes');
                $table->foreign('id_docente3')->references('recordId')->on('docentes');
                $table->foreign('id_docente4')->references('recordId')->on('docentes');
                $table->foreign('id_docente5')->references('recordId')->on('docentes');
                $table->foreign('id_docente6')->references('recordId')->on('docentes');
                $table->foreign('id_docente7')->references('recordId')->on('docentes');
                $table->foreign('id_docente8')->references('recordId')->on('docentes');
                $table->foreign('id_docente9')->references('recordId')->on('docentes');
                $table->foreign('id_docente10')->references('recordId')->on('docentes');

//								$table->foreign('webinar1_id')->references('recordId')->on('webinars');
//								$table->foreign('webinar2_id')->references('recordId')->on('webinars');
//								$table->foreign('webinar3_id')->references('recordId')->on('webinars');
//								$table->foreign('webinar4_id')->references('recordId')->on('webinars');
//								$table->foreign('webinar5_id')->references('recordId')->on('webinars');

                $table->foreign('id_categoria')->references('id')->on('categoria');
                $table->foreign('id_tipo')->references('id')->on('tipo');
        });

        Schema::create('blog', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('title', 255);
            $table->longText('descriptions');
            $table->longText('short_description');
            $table->integer('likes');
            $table->text('tags');
            $table->longText('image_web');
            $table->integer('interactions');
            $table->integer('popularity');
            $table->integer('comments');
            $table->unsignedBigInteger('product_id');
            $table->unsignedBigInteger('teacher_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('product_id')->references('id')->on('productos');
            $table->foreign('teacher_id')->references('recordId')->on('docentes');
        });

        Schema::create('comment', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('comment', 300);
            $table->string('name', 300);
            $table->string('email', 300);
            $table->boolean('validate');
            $table->unsignedBigInteger('blog_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('blog_id')->references('id')->on('blog');
        });

        Schema::create('resources', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('title', 300);
            $table->text('description');
            $table->string('link_img', 500);
            $table->string('link_download', 500);
            $table->unsignedBigInteger('product_id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('product_id')->references('id')->on('productos');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
