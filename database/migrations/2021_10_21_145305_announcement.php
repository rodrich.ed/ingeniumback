<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Announcement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('area', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->string('name', 255);
            $table->string('descriptions', 255);
            $table->timestamps();
            $table->softDeletes();

        });

        Schema::create('announcement', function (Blueprint $table){
            $table->engine ='InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->unsignedBigInteger('id_area')->nullable();
            $table->string('title',255);
            $table->longText('descriptions');
            $table->longText('requires');
            $table->longText('link');
            $table->integer('quantity');
            $table->string('place',100);
            $table->string('schedule',100);
            $table->string('state',1);

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('id_area')->references('id')->on('area');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
